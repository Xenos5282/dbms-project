/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbms_proj;

import dbms_proj.connection.connect;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MAHE
 */
public class admin_addCategory extends javax.swing.JFrame {

    /**
     * Creates new form admin_addCategory
     */
    
       
    public admin_addCategory() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgCategorytype = new javax.swing.ButtonGroup();
        lblAddCategory = new javax.swing.JLabel();
        lblCategoryname = new javax.swing.JLabel();
        tfCategoryname = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        lblCategorytype = new javax.swing.JLabel();
        lblDesc = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taDesc = new javax.swing.JTextArea();
        rbSupporting = new javax.swing.JRadioButton();
        rbOpen = new javax.swing.JRadioButton();
        rbCultural = new javax.swing.JRadioButton();
        btnBack = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblAddCategory.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblAddCategory.setText("ADD CATEGORY");

        lblCategoryname.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblCategoryname.setText("Category Name:");

        tfCategoryname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfCategorynameActionPerformed(evt);
            }
        });

        jButton1.setText("ADD");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        lblCategorytype.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblCategorytype.setText("Category Type:");

        lblDesc.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblDesc.setText("Description:");

        taDesc.setColumns(20);
        taDesc.setRows(5);
        jScrollPane1.setViewportView(taDesc);

        bgCategorytype.add(rbSupporting);
        rbSupporting.setText("SUPPORTING");

        bgCategorytype.add(rbOpen);
        rbOpen.setText("OPEN");
        rbOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbOpenActionPerformed(evt);
            }
        });

        bgCategorytype.add(rbCultural);
        rbCultural.setText("CULTURAL");

        btnBack.setText("BACK");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(75, 75, 75)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCategorytype)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(rbCultural)
                        .addGap(10, 10, 10)
                        .addComponent(rbSupporting)
                        .addGap(18, 18, 18)
                        .addComponent(rbOpen))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(lblDesc, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(lblCategoryname)
                                    .addGap(18, 18, 18)
                                    .addComponent(tfCategoryname, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(jButton1)
                                .addGap(44, 44, 44)
                                .addComponent(btnBack)))
                        .addGap(4, 4, 4)))
                .addContainerGap(112, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblAddCategory, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(137, 137, 137))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAddCategory)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfCategoryname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCategoryname))
                .addGap(23, 23, 23)
                .addComponent(lblCategorytype)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbCultural)
                    .addComponent(rbSupporting)
                    .addComponent(rbOpen))
                .addGap(14, 14, 14)
                .addComponent(lblDesc, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(btnBack))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        
        String cat_name = tfCategoryname.getText();
        String cat_desc = taDesc.getText();
        String cat_type = null; 
        
        //Check if fields are empty
        boolean validDetails = true;
        
        //Assign cat_type from radio buttons
        if(rbCultural.isSelected()){
            cat_type="CULTURAL";
        }
        else if(rbSupporting.isSelected()){
            cat_type="SUPPORTING";
        }
        else if(rbOpen.isSelected()){
            cat_type="OPEN";
        }
        //Check if fields are filled
        if(cat_name.isEmpty())
        {
            new alert("Category Name cannot be empty");
            validDetails = false;
        }
        //Check Category Type
        else if(cat_type == null){
          
                new alert("Please select a Category Type");
                validDetails = false;
          
        }
        else if(cat_desc.trim().isEmpty()){
            new alert("Category Description cannot be empty");                    
            validDetails = false;
        }   
        
        if(validDetails == true){
            //SQL Connection
            boolean noDupDetails = true;
            String namechk = cat_name.toLowerCase();
            connect conn = new connect();
            PreparedStatement PrSt;
            ResultSet rs;
            String query;
            try {                
                

                query="select count(*) from categories where lower(name)=? and type=?";
            
                PrSt=conn.con.prepareStatement(query);
                PrSt.setString(1, namechk);
                PrSt.setString(2, cat_type);
                rs=PrSt.executeQuery();
                rs.next();
                if(rs.getInt(1)>0){
                    new alert("Category with same name and type already exists");
                    noDupDetails=false;
                }
            } catch (SQLException ex) {
                System.out.println(ex);
            }
            
            if(noDupDetails==true){
                //SQL Connection
                int cat_id;
                try {
                    query="select max(id) from categories";
                    PrSt = conn.con.prepareStatement(query);
                    rs=PrSt.executeQuery();
                    rs.next();
                    cat_id=rs.getInt(1)+1;
                    System.out.println(cat_id);
                    
                    query="insert into categories values(?,?,?,?)";
                    PrSt = conn.con.prepareStatement(query);
                    PrSt.setInt(1, cat_id);
                    PrSt.setString(2,cat_name);
                    PrSt.setString(3, cat_type);
                    PrSt.setString(4, cat_desc);
                    int ret = PrSt.executeUpdate();
                    if(ret==1){
                        new alert("Category added successfully");
                    }
                    else{
                        new alert("Error while adding category");
                    }
                } catch (SQLException ex) {
                    System.out.println(ex);
                }
                
                
                
                
            }
            
          
        }
        
        
        
        
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void tfCategorynameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfCategorynameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfCategorynameActionPerformed

    private void rbOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbOpenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbOpenActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        
        admin_home adHome = new admin_home();
        adHome.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(admin_addCategory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(admin_addCategory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(admin_addCategory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(admin_addCategory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new admin_addCategory().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgCategorytype;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblAddCategory;
    private javax.swing.JLabel lblCategoryname;
    private javax.swing.JLabel lblCategorytype;
    private javax.swing.JLabel lblDesc;
    private javax.swing.JRadioButton rbCultural;
    private javax.swing.JRadioButton rbOpen;
    private javax.swing.JRadioButton rbSupporting;
    private javax.swing.JTextArea taDesc;
    private javax.swing.JTextField tfCategoryname;
    // End of variables declaration//GEN-END:variables
}
